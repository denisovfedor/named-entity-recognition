package javaclient.server.executor;

import java.io.Closeable;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;

import com.sun.istack.internal.NotNull;

import javaclient.server.database.DatabaseDAO;
import javaclient.server.recogniser.RecogniserContainer;
import javaclient.server.recogniser.RecogniserPool;

public class TaskExecutor implements Closeable {

    private final ExecutorService executor = Executors.newFixedThreadPool(10);
    private final RecogniserPool pool;
    private final DatabaseDAO databaseDAO;

    public TaskExecutor(@NotNull DatabaseDAO databaseDAO, @NotNull String serverExecutablePath) {
        Validate.notNull(databaseDAO);

        this.pool = new RecogniserPool(serverExecutablePath);
        this.databaseDAO = databaseDAO;
    }

    public void submit(String path2File) {
        Callable<String> callableTask = () -> execute(path2File);
        executor.submit(callableTask);
        System.out.println("Submit task -" + path2File);
    }

    @Nullable
    private String execute(String pathToFile) {

        String result = databaseDAO.get(pathToFile);
        if (result != null) {
            System.out.println("DEBUG - result for " + pathToFile + " already in DB - skipping");
            return result;
        }


        RecogniserContainer container = pool.borrow();
        if (container == null) {
            System.out.println("ERROR - fail to start server - skipping processing - " + pathToFile);
            return null;
        }
        try {
            result = container.process(pathToFile);
            System.out.println(result);
            databaseDAO.insert(pathToFile, result);
        } finally {
            pool.returnAndCheckError(container);
        }
        return result;
    }


    @Override
    public void close() {
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            System.out.println("Fail to process all tasks due to interrupted exception");
        }

        try {
            executor.shutdown();
            pool.destroy();
            pool.close();
        } catch (Exception e) {
            System.out.println("ERROR - fail to close TaskExecutor due to " + e.getMessage());
        }
    }
}