package javaclient.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import javaclient.server.database.DatabaseDAO;
import javaclient.server.executor.TaskExecutor;

public class Main {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("3 arguments required : pathToDir - path to directory with messages," +
                    "pathToDB - path for DB storage," +
                    "pathToPython - path to Main.py");
            return;
        }

        String pathToDir = args[0];
        String pathToDB = args[1];
        String pathToPython = args[2];

        DatabaseDAO databaseDAO = new DatabaseDAO(pathToDB);

        TaskExecutor taskExecutor = null;
        try{
            System.out.println("Start indexing files in dir - " + pathToDir);
            List<String> fullPathToFiles = getFilesInDirRecursive(pathToDir);
            System.out.println("Got " + fullPathToFiles.size() + " files");
            taskExecutor = new TaskExecutor(databaseDAO,pathToPython);
            fullPathToFiles.forEach(taskExecutor::submit);
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if (taskExecutor != null) {
                taskExecutor.close();
            }
        }
    }

    private static List<String> getFilesInDirRecursive(String path) throws IOException {
        return Files.walk(Paths.get(path))
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());
    }
}
