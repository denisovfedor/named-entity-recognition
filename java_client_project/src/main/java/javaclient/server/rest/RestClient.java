package javaclient.server.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;

import com.sun.istack.internal.NotNull;

public class RestClient {

    private final HttpClient client = HttpClientBuilder.create().build();
    private final String urlRest;

    public RestClient(int port) {
        urlRest = "http://localhost:" + port;
    }

    public boolean check() {
        String line;
        try {
            HttpGet request =
                    new HttpGet(urlRest + "/check");
            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                System.out.print("ERROR");
                return false;
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            line = rd.readLine();
        } catch (IOException e) {
            System.out.println("Fail to check status");
            return false;
        }
        System.out.println("STATUS - " + line);
        return "OK".equals(line);
    }

    @Nullable
    public String process(@NotNull String pathToFile) {
        Validate.notNull(pathToFile);

        String line = null;
        try {
            HttpGet request =
                    new HttpGet(urlRest + "/process?path=" + pathToFile);
            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                System.out.print("ERROR");
                return null;
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            line = rd.readLine();
            if (line.startsWith("ERROR")) {
                System.out.print("Error ---- " + line);
                return null;
            }
        } catch (IOException e) {
            System.out.print(e.getMessage());
        }
        return line;
    }

    /**
     * Посылает сервису команду остановиться и завершиться.
     */
    public void sendExit() {
        HttpPost request = new HttpPost(urlRest + "/shutdown");
        HttpResponse response;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }
        if (response.getStatusLine().getStatusCode() != 200) {
            System.out.println("ERROR - Fail to shutdown python server");
        } else {
            System.out.println("INFO - Success python server shutdown");
        }
    }

}
