package javaclient.server.recogniser;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;

import com.sun.istack.internal.NotNull;

import javaclient.server.rest.RestClient;

public class RecogniserFactory {

    private final String executable_path;

    public RecogniserFactory(@NotNull String executable_path) {
        Validate.notNull(executable_path);

        this.executable_path = executable_path;
    }
    private final List<Integer> PORTS_FOR_USAGE = IntStream.range(5500, 5600).boxed().collect(Collectors.toList());
    private final int RETRY_PORTS_NUM = 10;
    private int lastUsedPortIdx = 0;

    @Nullable
    public synchronized RecogniserContainer createRecogniser() {
        Process process = null;
        RestClient restClient = null;
        int numTry = 0;
        while (process == null || !process.isAlive() || restClient == null || !restClient.check()) {
            if (PORTS_FOR_USAGE.size() < lastUsedPortIdx) {
                System.out.println("ERROR - there is no ports to try in defined range");
                return null;
            }
            int port = PORTS_FOR_USAGE.get(lastUsedPortIdx);
            System.out.println("Starting server on " + port);

            ProcessBuilder server = new ProcessBuilder(
                    "python3",
                    executable_path,
                    String.valueOf(port))
                    .redirectErrorStream(true);

            try {
                process = server.start();
                restClient = initRestConnection(port);
            } catch (IOException e) {
                System.out.println("INFO - Fail to client or start server on port due to " + e.getMessage());
            }
            lastUsedPortIdx++;
            numTry++;
            if (numTry > RETRY_PORTS_NUM) {
                break;
            }
        }

        if (process == null || !process.isAlive() || !restClient.check()) {
            System.out.println("ERROR - Fail to start server on range of ports");
            return null;
        }

        return new RecogniserContainer(process, restClient);
    }

    @Nullable
    public RestClient initRestConnection(int port) {
        RestClient restClient = new RestClient(port);
        int i = 0;
        while (!restClient.check()) {
            System.out.println("Waiting for server REST to startup");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (i > 20) {
                return null;
            }
            i++;
        }
        return restClient;
    }
}
