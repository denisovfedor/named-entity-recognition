package javaclient.server.recogniser;

import org.apache.commons.lang3.Validate;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.sun.istack.internal.NotNull;

import javaclient.server.pool.PoolBorrowException;
import javaclient.server.pool.PoolableRecogniserFactory;

public class RecogniserPool extends GenericObjectPool {


    public RecogniserPool(@NotNull String executable_path) {
        super(new PoolableRecogniserFactory(executable_path), createConfig());
    }

    private static Config createConfig() {
        Config config = new Config();
        config.minIdle = 1;
        config.maxActive = 10;
        config.maxWait = 5000;
        config.testOnBorrow = false;
        config.testOnReturn = false;
        config.whenExhaustedAction = WHEN_EXHAUSTED_BLOCK;
        return config;
    }

    @NotNull
    public RecogniserContainer borrow() {
        try {
            return (RecogniserContainer) borrowObject();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return null;
        } catch (Exception e) {
            throw new PoolBorrowException(e);
        }
    }

    public void returnAndCheckError(@NotNull RecogniserContainer container) {
        Validate.notNull(container);
        try {
            returnObject(container);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            throw new PoolBorrowException(e);
        }
    }


    public void destroy() {
        try {
            close();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            throw new PoolBorrowException(e);
        }
    }


}
