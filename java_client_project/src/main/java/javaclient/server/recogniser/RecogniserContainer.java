package javaclient.server.recogniser;

import java.io.Closeable;
import java.io.IOException;

import org.apache.commons.lang3.Validate;

import com.sun.istack.internal.NotNull;

import javaclient.server.rest.RestClient;

public class RecogniserContainer implements Closeable {
    private final Process process;
    private final RestClient client;

    public RecogniserContainer(@NotNull Process process,
                               @NotNull RestClient client) {
        Validate.notNull(process);
        Validate.notNull(client);

        this.process = process;
        this.client = client;
    }

    public String process(String command) {
        return client.process(command);
    }

    public boolean check() {
        return process.isAlive() && client.check();
    }


    @Override
    public void close() throws IOException {
        client.sendExit();
    }
}
