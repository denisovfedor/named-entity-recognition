package javaclient.server.database;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;

import com.sun.istack.internal.NotNull;

import io.jsondb.JsonDBTemplate;

public class DatabaseDAO {

    private final JsonDBTemplate jsonDBTemplate;

    public DatabaseDAO(String dbFilesLocation) {
        String baseScanPackage = "javaclient.server.database"; // POJO package
        jsonDBTemplate = new JsonDBTemplate(dbFilesLocation, baseScanPackage);
        if (!jsonDBTemplate.collectionExists(RecordDTO.class)) {
            jsonDBTemplate.createCollection(RecordDTO.class);
        }
    }

    public void insert(@NotNull String pathToFile, @NotNull String result) {
        Validate.notNull(pathToFile);
        Validate.notNull(result);

        final RecordDTO recordDTO = new RecordDTO();
        recordDTO.setMessageId(pathToFile);
        recordDTO.setRecognitionResultJSON(result);
        try {
            jsonDBTemplate.insert(recordDTO);
        } catch (RuntimeException e) {
            System.out.println("ERROR - fail to insert to DB - " + e.getMessage());
        }
    }

    @Nullable
    public String get(String messageId) {
        RecordDTO result = null;
        try {
            result = jsonDBTemplate.findById(messageId, "records");
        } catch (RuntimeException e) {
            System.out.println("ERROR - fail to insert to DB - " + e.getMessage() );
        }
        return result == null ? null : result.getRecognitionResultJSON();
    }
}
