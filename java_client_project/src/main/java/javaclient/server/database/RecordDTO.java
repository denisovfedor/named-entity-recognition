package javaclient.server.database;

import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

@Document(collection = "records", schemaVersion = "1.0")
public class RecordDTO {
    @Id
    private String messageId;

    private String recognitionResultJSON;

    public String getMessageId() {
        return messageId;
    }

    public String getRecognitionResultJSON() {
        return recognitionResultJSON;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setRecognitionResultJSON(String recognitionResultJSON) {
        this.recognitionResultJSON = recognitionResultJSON;
    }
}
