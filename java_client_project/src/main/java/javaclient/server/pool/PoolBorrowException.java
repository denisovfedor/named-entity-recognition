package javaclient.server.pool;

public class PoolBorrowException extends RuntimeException {

    public PoolBorrowException(Throwable cause) {
        super("Failed to borrow object from pool", cause);
    }
}
