package javaclient.server.pool;

import org.apache.commons.pool.PoolableObjectFactory;

import com.sun.istack.internal.NotNull;

import javaclient.server.recogniser.RecogniserContainer;
import javaclient.server.recogniser.RecogniserFactory;

public class PoolableRecogniserFactory implements PoolableObjectFactory {
    private final RecogniserFactory factory;

    public PoolableRecogniserFactory(@NotNull String executable_path) {
        this.factory = new RecogniserFactory(executable_path);
    }

    @Override
    public Object makeObject() throws Exception {
        return factory.createRecogniser();
    }

    @Override
    public void destroyObject(Object obj) throws Exception {
        ((RecogniserContainer) obj).close();
    }

    @Override
    public boolean validateObject(Object obj) {
        return ((RecogniserContainer) obj).check();
    }

    @Override
    public void activateObject(Object obj) {

    }

    @Override
    public void passivateObject(Object obj) {

    }
}