import spacy
import json
import math
import sys

from flask import Flask
from flask import request

nlp = spacy.load('en_core_web_sm')


def read_file(path_2_file):
    lines = []
    header_ended = False
    with open(path_2_file) as file:
        for file_line in file:
            file_line = file_line.strip('\n \t')
            if file_line == "":
                header_ended = True
                continue

            if header_ended:
                lines.append(file_line)
    return lines


def get_file_idx(path_2_file):
    split_path = path_2_file.split('/')
    file_name = split_path[len(path_2_file.split('/')) - 1]
    return file_name[:len(file_name) - 1]


def recognize_named_entities(batches):
    named_entities_dict = {}
    for batch in batches:
        doc = nlp('\n'.join(batch))
        for ent in doc.ents:
            label = ent.label_
            text = ent.text.strip('\n \t')
            entity_info = (text, ent.start_char, ent.end_char)
            if label in named_entities_dict:
                named_entities_dict[label].append(entity_info)
            else:
                named_entities_dict[label] = [entity_info]
    return named_entities_dict


def slice_text_on_batches(file_lines):
    batch_size = 25
    batch_amount = math.ceil(len(file_lines) / batch_size) # round up amount of batches (11.3 -> 12)
    batches = []
    for i in range(batch_amount):
        batches.append(file_lines[i*batch_size : (i+1)*batch_size])
    return batches


def format_output(named_entities_dict, path_to_file):
    file_idx = get_file_idx(path_to_file)
    data = {file_idx: named_entities_dict}
    return json.dumps(data)


def process(path_to_file):
    try:
        file_lines = read_file(path_to_file)
    except OSError as e:
        return "ERROR - " + e.strerror + " - " + e.filename
    if not file_lines:
        return "ERROR - empty file - " + path_to_file

    batches = slice_text_on_batches(file_lines)
    named_entities_dict = recognize_named_entities(batches)

    return format_output(named_entities_dict, path_to_file)


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


app = Flask(__name__)


@app.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'


@app.route('/check')
def rest_check():
    return "OK"


@app.route('/process')
def rest_process():
    path = request.args.get('path')
    if path == '':
        return "ERROR"
    return process(path)


if len(sys.argv) != 2:
    raise RuntimeError('Python server script ' + sys.argv[0] + ' expect 1 argument - port')

app.run(host='localhost', port=sys.argv[1])
