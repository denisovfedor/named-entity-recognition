## Named entity recognition
* java client runs multiple python processes, use them as workers for NER
* interract with them using REST
* stores results in DB with path_to_file as primary key and assosiated with it result in JSON format

### How to run
Complile java and run with tree args: pathToDir - path to directory with message, pathToDB - path to Database Storage, pathToPython - path to Main.py

### How it works
Java search for text files in pathToDir recursively in all dirs, submit tasks to analyse them to executor pool for async execution.

During the task execution one of a free healthy python server recives command to recognise named enitites in text file by REST.

Path to text file passed as argument to REST method. Java recieve result of recognition in string in JSON format synchronously waiting for it. 

After that result is stored in DB with path_to_file as primary key and assosiated with it result in JSON format.

### Note
Before recognition by the spaCy model header of a text file is croped (by simple rule - rm all lines before empty one)

